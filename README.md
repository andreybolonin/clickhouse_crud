Installation
=================

1) git clone git@gitlab.com:andreybolonin/clickhouse_crud.git

2) composer install

3) sudo docker run -d --name clickhouse-server -p 127.0.0.1:8123:8123 --ulimit nofile=262144:262144 yandex/clickhouse-server

4) bin/console server:start

Links
=================

https://git-scm.com/book/ru/v2/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5-%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git

https://clickhouse.yandex/benchmark.html

https://hub.docker.com/r/yandex/clickhouse-server/