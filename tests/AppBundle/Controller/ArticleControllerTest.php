<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleControllerTest extends WebTestCase
{

   public function testIndexAction()
    {
        //$this->markTestSkipped();
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        // есть надпись "Список объектов" в заголовке страницы
        $this->assertContains('Список объектов', $crawler->filter('body > h1')->text());

        // в таблице 0 строк и 0 ячеек
        $this->assertCount(0, $crawler->filter('body > table > tbody > tr'));
        $this->assertCount(0, $crawler->filter('body > table > tbody > tr > td'));
    }

    public function testNewAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET','/new');

        $form = $crawler->selectButton('article[save]')->form();
        $form['article[name]'] = 'test 888';
        $form['article[description]'] = 'testing 888';
        $client->submit($form);

        // в таблице 1 строка и 5 ячеек
        $crawler = $client->request('GET', '/');
        $this->assertCount(1, $crawler->filter('body > table > tbody > tr'));
        $this->assertCount(5, $crawler->filter('body > table > tbody > tr > td'));

        // в таблице есть созданная строка
        $this->assertContains('test 888', $crawler->filter('body > table > tbody')->text());
        $this->assertContains('testing 888', $crawler->filter('body > table > tbody')->text());
    }

    public function testEditAction()
    {
        sleep(1);

        $client = static::createClient();

        $crawlerStart = $client->request('GET', '/');
        $linkEdit = $crawlerStart->filter('tr:contains("test 888")')
            ->children()
            ->filter('a:contains("Edit")')
            ->link();

        $crawler = $client->request('GET', $linkEdit->getUri());

        $form = $crawler->selectButton('article[save]')->form();
        $form['article[name]'] = 'test 999';
        $form['article[description]'] = 'testing 999';
        $client->submit($form);

        $crawler = $client->request('GET', '/');

        // в таблице 1 строка и 5 ячеек
        $this->assertCount(1, $crawler->filter('body > table > tbody > tr'));
        $this->assertCount(5, $crawler->filter('body > table > tbody > tr > td'));

        // в таблице есть обновленная строка
        $this->assertContains('test 999', $crawler->filter('body > table > tbody')->text());
        $this->assertContains('testing 999', $crawler->filter('body > table > tbody')->text());
    }

    public function testDeleteAction()
    {
        sleep(1);

        $client = static::createClient();

        $crawlerStart = $client->request('GET', '/');
        $linkDelete = $crawlerStart->filter('tr:contains("test 999")')
            ->children()
            ->filter('a:contains("Delete")')
            ->link();

        $client->request('GET', $linkDelete->getUri());

        $crawler = $client->request('GET', '/');

        // в таблице 0 строк и 0 ячеек
        $this->assertCount(0, $crawler->filter('body > table > tbody > tr'));
        $this->assertCount(0, $crawler->filter('body > table > tbody >tr > td'));

        // в таблице нет обновленной строки (удалена)
        $this->assertNotContains('test 999', $crawler->filter('body > table > tbody')->text());
        $this->assertNotContains('testing 999', $crawler->filter('body > table > tbody')->text());
    }
}
