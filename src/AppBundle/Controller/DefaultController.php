<?php

namespace AppBundle\Controller;

use AppBundle\Form\ArticleType;
use AppBundle\Entity\Article;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOD\DBALClickHouse\Connection;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     *
     * @param $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $conn = $this->get('doctrine.dbal.clickhouse_connection');

        $sql = "SELECT * 
                FROM article 
                ORDER BY created_at DESC LIMIT 1 BY id";

        $articles = $conn->fetchAll($sql);

        return $this->render('default/index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * @Route("/new", name="new")
     *
     * @param $request
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $conn = $this->get('doctrine.dbal.clickhouse_connection');

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $sql = "INSERT 
                    INTO article 
                    VALUES (:id, 
                            :name, 
                            :description, 
                            :created_at, 
                            :created_date)";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue('id', $id = Uuid::uuid1()->toString());
            $stmt->bindValue('name', $article->getName());
            $stmt->bindValue('description', $article->getDescription());
            $stmt->bindValue('created_at', time());
            $stmt->bindValue('created_date', date("Y-m-d", time()));
            $stmt->execute();

            return $this->redirectToRoute('index');
        }

        return $this->render('default/form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param $request
     * @return Response|RedirectResponse
     */
    public function editAction(Request $request)
    {
        $conn = $this->get('doctrine.dbal.clickhouse_connection');

        $sql = 'SELECT *
                FROM article
                WHERE id = :id 
                ORDER BY created_at DESC LIMIT 1';

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id', $request->get('id'));
        $stmt->execute();
        $article_row = $stmt->fetchAll();

        $article = new Article();
        $article->setId($article_row[0]['id']);
        $article->setName($article_row[0]['name']);
        $article->setDescription($article_row[0]['description']);
        $article->setCreatedAt(new \DateTime ($article_row[0]['created_at']));

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if($editForm->isSubmitted() && $editForm->isValid())
        {
            $sql = "INSERT 
                    INTO article 
                    VALUES (:id, 
                            :name, 
                            :description, 
                            :created_at, 
                            :created_date)";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue('id', $article->getId());
            $stmt->bindValue('name', $article->getName());
            $stmt->bindValue('description', $article->getDescription());
            $stmt->bindValue('created_at', time());
            $stmt->bindValue('created_date', date("Y-m-d", time()));
            $stmt->execute();

            return $this->redirectToRoute('index');
        }

        return $this->render('default/form.html.twig', array(
            'form' => $editForm->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="delete")
     *
     * @param $request
     * @return RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $conn = $this->get('doctrine.dbal.clickhouse_connection');

        $sql = "INSERT INTO article 
                VALUES (:id , 
                        :name, 
                        :description, 
                        :created_at, 
                        :created_date)";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('id', $request->get('id'));
        $stmt->bindValue('name', '');
        $stmt->bindValue('description', '');
        $stmt->bindValue('created_at', time());
        $stmt->bindValue('created_date', date("Y-m-d", time()));
        $stmt->execute();

        return $this->redirectToRoute('index');
    }
}
