<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ClickhouseCommand
 *
 * @package AppBundle\Command
 */
class ClickhouseSchemaUpdateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('clickhouse:schema:update')
            // the short description shown while running "php bin/console list"
            ->setDescription('Create new table')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to create table ...");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clickhouse = $this->getContainer()->get('doctrine.dbal.clickhouse_connection');
        $clickhouse->exec('CREATE TABLE clickhouse_crud.article(
                                id String,
                                name String,
                                description String,
                                created_at DateTime,
                                created_date Date
                      ) ENGINE = MergeTree(created_date, (id, created_date), 8192);');
    }
}
