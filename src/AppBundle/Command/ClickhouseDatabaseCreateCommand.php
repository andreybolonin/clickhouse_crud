<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ClickhouseDatabaseCreateCommand
 *
 * @package AppBundle\Command
 */
class ClickhouseDatabaseCreateCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('clickhouse:database:create')
            // the short description shown while running "php bin/console list"
            ->setDescription('Create new database')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("This command allows you to create database ...");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clickhouse = $this->getContainer()->get('doctrine.dbal.clickhouse_connection');
        $clickhouse->exec('CREATE DATABASE clickhouse_crud;');
    }
}
